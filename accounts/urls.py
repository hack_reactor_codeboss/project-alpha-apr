from django.urls import path
from accounts.views import user_login, logout_user, signup_user


urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", logout_user, name="logout"),
    path("signup/", signup_user, name="signup"),
]
