from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required


@login_required
def list_projects(request):
    context = {"projects": Project.objects.filter(owner=request.user)}
    return render(request, "projects/projects_list.html", context)


@login_required
def show_project(request, id):
    context = {
        "project": get_object_or_404(Project, id=id),
    }
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    # if the request type is "POST"
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        # if the request type is "GET"
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
