from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


@login_required
def create_task(request):
    # if the request type is "POST"
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        # if the request type is "GET"
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def list_tasks(request):
    context = {"tasks": Task.objects.filter(assignee=request.user)}
    return render(request, "tasks/list_tasks.html", context)
